package ru.tim.sbt.news.service;

import ru.tim.sbt.news.model.ResponseBodyNews;

import java.util.Date;
import java.util.List;


public interface AppServiceImpl {
    public void createNews(Integer id, String title, String content, Date creation_date, Integer creator_id);

    public List<ResponseBodyNews> get_all_news();

    public ResponseBodyNews get_one_news(Integer id);

    public void deleteNews(Integer id);
}
