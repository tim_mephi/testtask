package ru.tim.sbt.news.model;


import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "creator")

public class TableCreator implements Serializable {

    @Id
    @Type(type = "integer")
    private Integer id;

    @Column(name = "fio")
    private String fio;

    protected TableCreator() {
    }

    public TableCreator(Integer id, String fio) {
        this.id = id;
        this.fio = fio;
    }

    @Override
    public String toString() {
        return getId().toString();
    }
}
