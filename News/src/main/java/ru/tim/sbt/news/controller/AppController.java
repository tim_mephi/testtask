package ru.tim.sbt.news.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tim.sbt.news.model.*;
import ru.tim.sbt.news.service.AppServiceImpl;

import java.util.*;
import java.util.List;


@RestController

public class AppController {
    @Autowired
    private AppServiceImpl service;


    @PostMapping(value = "/create_news")
    @ResponseBody
    public ResponseEntity create_news(@RequestBody RequestBodyNews body) {
        try {
            service.createNews(body.getId(), body.getTitle(), body.getContent(), body.getCreation_date(), body.getCreator_id());
            return ResponseEntity.status(HttpStatus.OK).body("OK");
        } catch (NoSuchElementException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("BAD REQUEST: " + ex.getMessage() + " (there is no such Creator)");
        }

    }

    @PostMapping(value = "/delete_news")
    @ResponseBody
    public ResponseEntity delete_news(@RequestBody BodyForIdRequest body) {
        try {
            service.deleteNews(body.getId());
            return ResponseEntity.status(HttpStatus.OK).body("OK");
        } catch (EmptyResultDataAccessException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("BAD REQUEST: " + ex.getMessage());
        }

    }

    @GetMapping(value = "/get_all_news")
    @ResponseBody
    public ResponseEntity get_all_news() {
        List<ResponseBodyNews> result = service.get_all_news();
        if (!result.isEmpty())
            return ResponseEntity.status(HttpStatus.OK).body(result);
        else
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("NO CONTENT");
    }

    @PostMapping(value = "/get_one_news")
    public ResponseEntity get_one_news(@RequestBody BodyForIdRequest body) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.get_one_news(body.getId()));
        } catch (NoSuchElementException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("BAD REQUEST: " + ex.getMessage());
        }
    }


}
