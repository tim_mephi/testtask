package ru.tim.sbt.news.model;


import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Table(name = "news")

public class TableNews implements Serializable {
    @Id
    @Type(type = "integer")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creation_date;


    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "creator_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TableCreator tableCreator;

    protected TableNews() {
    }

    public TableNews(Integer id, String title, String content, Date creation_date) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creation_date = creation_date;
    }

    public ResponseBodyNews prepareResponse() {
        return new ResponseBodyNews(getId(), getTitle(), getContent(), getCreation_date(), this.getTableCreator().getFio());

    }
}
