package ru.tim.sbt.news.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tim.sbt.news.model.ResponseBodyNews;
import ru.tim.sbt.news.model.TableCreator;
import ru.tim.sbt.news.model.TableNews;
import ru.tim.sbt.news.repository.NewsRepository;
import ru.tim.sbt.news.repository.AuthorRepository;

import java.util.*;

@Service
public class AppAppService implements AppServiceImpl {
    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private AuthorRepository authorRepository;

    /**
     * В данном сервисе не учитывается то, что в бд уже может находится новость с существующим id,
     * задание понято так, что если такой id есть, то данные перезаписываются
     **/

    public void createNews(Integer id, String title, String content, Date creation_date, Integer creator_id) {
        Optional<TableCreator> authors = authorRepository.findById(creator_id);
        TableNews tableNews = new TableNews(id, title, content, creation_date);
        tableNews.setTableCreator(authors.get());
        newsRepository.save(tableNews);
    }

    public List<ResponseBodyNews> get_all_news() {
        List<ResponseBodyNews> result = new LinkedList<ResponseBodyNews>();
        for (TableNews item : newsRepository.findAll())
            result.add(item.prepareResponse());
        result.sort(Comparator.comparingInt(ResponseBodyNews::getId));
        return result;
    }

    public ResponseBodyNews get_one_news(Integer id) {
        return newsRepository.findById(id).get().prepareResponse();
    }

    public void deleteNews(Integer id) {
        newsRepository.deleteById(id);
    }

}
