package ru.tim.sbt.news.model;

import lombok.Data;

import java.util.Date;


@Data
public class RequestBodyNews {

    private Integer id;

    private String title;

    private String content;

    private Date creation_date;

    private Integer creator_id;

}
