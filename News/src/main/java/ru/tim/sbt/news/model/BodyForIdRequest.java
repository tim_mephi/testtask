package ru.tim.sbt.news.model;

import lombok.Data;


@Data
public class BodyForIdRequest {
    private int id;

}
