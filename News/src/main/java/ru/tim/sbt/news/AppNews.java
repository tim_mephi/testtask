package ru.tim.sbt.news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AppNews {
    public static void main(String[] args) {
        SpringApplication.run(AppNews.class, args);
    }
}
