package ru.tim.sbt.news.model;

import lombok.Data;

import java.util.Date;


@Data
public class ResponseBodyNews {
    private Integer id;
    private String title;
    private String content;
    private Date creation_date;
    private String fio;

    public ResponseBodyNews(Integer id, String title, String content, Date creation_date, String fio) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creation_date = creation_date;
        this.fio = fio;
    }
}
