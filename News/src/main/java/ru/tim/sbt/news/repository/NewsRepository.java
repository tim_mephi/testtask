package ru.tim.sbt.news.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tim.sbt.news.model.TableNews;

@Repository
public interface NewsRepository extends JpaRepository<TableNews, Integer> {
}